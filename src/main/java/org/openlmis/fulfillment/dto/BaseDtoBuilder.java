/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.fulfillment.dto;

public abstract class BaseDtoBuilder {

  public static final String PLANT = "plant";
  public static final String COMPANY = "company";
  public static final String COMPANY_VALUE = "MSD-FIN";
  public static final String ELMIS_ORDER_NUM = "eLMISOrderNum";
  public static final String FACILITY_ID = "facilityID";
  public static final String CUSTOMER_ID = "customerID";
  public static final String CURRENCY = "currency";
  public static final String CURRENCY_VALUE = "TZS";
  public static final String PERIOD = "period";
  public static final String QUOTE_DATE = "quoteDate";
  public static final String ERROR = "error";
  public static final String QUOTE_NUMBER = "quoteNumber";
  public static final String STATUS = "status";
  public static final String STATUS_PENDING = "Pending";
  public static final String PRICE_LIST_CODE = "priceListCode";
  public static final String QUOTE_LINE = "quoteLine";
  public static final String F_AUTHORIZED_BY = "fAuthorizedBy";
  public static final String F_AUTHORIZED_DATE = "fAuthorizedDate";
  public static final String D_APPROVED_BY = "dApprovedBy";
  public static final String D_APPROVED_DATE = "dApprovedDate";
  public static final String R_APPROVED_BY = "rApprovedBy";
  public static final String R_APPROVED_DATE = "rApprovedDate";
  public static final String EMERGENCY = "emergency";
  public static final String RNR_ID = "rnrId";
  public static final String FAC_AUTHORIZED_BY = "facAuthorizedBy";
  public static final String FAC_AUTHORIZED_DATE = "facAuthorizedDate";
  public static final String DIST_APPROVED_BY = "distApprovedBy";
  public static final String DIST_APPROVED_DATE = "distApprovedDate";
  public static final String REG_APPROVED_BY = "regApprovedBy";
  public static final String REG_APPROVED_DATE = "regApprovedDate";


}
