/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org.
 */

package org.openlmis.fulfillment.service.requisition;

import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.openlmis.fulfillment.dto.RequisitionDtoV2;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
public class Requisition2Service extends
    BaseRequisitionService<RequisitionDtoV2> {

  private static final Logger LOGGER = LoggerFactory.getLogger(
      Requisition2Service.class);

  public static final String ACCESS_TOKEN = "access_token";

  @Value("${requisition.url}")
  private String requisitionUrl;

  /**
   * Return one object from Reference data service.
   *
   * @param id UUID of requesting object.
   * @return Requesting reference data object.
   */
  public RequisitionDtoV2 findOne(UUID id) {
    String url = getServiceUrl() + getUrl() + id;

    try {
      ResponseEntity<RequisitionDtoV2> responseEntity = restTemplate.exchange(
          buildUri(url), HttpMethod.GET, createEntity(), getResultClass());
      return responseEntity.getBody();
    } catch (HttpStatusCodeException ex) {
      // rest template will handle 404 as an exception, instead of returning null
      if (ex.getStatusCode() == HttpStatus.NOT_FOUND) {
        LOGGER.warn("{} with id {} does not exist. ", getResultClass().getSimpleName(), id);
        return null;
      } else {
        throw buildDataRetrievalException(ex);
      }
    }
  }

  /**
   * Retrieves access token from the current HTTP context.
   *
   * @return token.
   */
  public String obtainUserAccessToken() {
    HttpServletRequest request = getCurrentHttpRequest();
    if (request == null) {
      return null;
    }

    String accessToken = request.getParameter(ACCESS_TOKEN);
    return accessToken;
  }

  private HttpServletRequest getCurrentHttpRequest() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes instanceof ServletRequestAttributes) {
      HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
      LOGGER.info("request url: {}", requisitionUrl);
      return request;
    }
    return null;
  }

  @Override
  protected String getServiceUrl() {
    return requisitionUrl;
  }

  @Override
  protected String getUrl() {
    return "/api/requisitions/";
  }

  @Override
  protected Class<RequisitionDtoV2> getResultClass() {
    return RequisitionDtoV2.class;
  }

  @Override
  protected Class<RequisitionDtoV2[]> getArrayResultClass() {
    return RequisitionDtoV2[].class;
  }

}

